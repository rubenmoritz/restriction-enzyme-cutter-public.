# README #

Welcome to the Restriction enzyme cutter by Ruben Moritz.
![resenz.png](https://bitbucket.org/repo/koLErd/images/763868430-resenz.png)




### What is this repository for? ###

This repository holds a restriction enzyme cutter that will cut any valid DNA string (only containing the characters ATCG or in lowercase) into fragments. This is done using a restiction enzyme of choice that can be selected in a dropdown box or typed in by the user. This string must contain at least one of the nucleotides (ATCG) and one slash forward (/) for determining the cut spot.
As long as the input is right any DNA sequence can be cut by any given restriction enzyme that is valid for above rules.
If one of these is not valid the go button will be deactivated and changed in color.
If the input is corrected the button will be activated again (on button up and/or on blur).

When the input is valid and the user will click the "GO" button the fragments will be calculated using a servlet.
The information will be send to the servlet by a Jquery and Ajax function (GET). This will be responded with a generated table that will hold all the information for every fragment that has been found.
If a user wants to test another sequence this table will be replaced with a new one.

### How do I get set up? ###

**-Needed files:**
-JDK is needed to compile.
-Tomcat is needed to compile and run.
-Netbeans is needed to potentially make compiling easy.

-Download the project here from the repository.

-Compile the project in either netbeans or by commandline to a .war.
If you want to use the commandline the following command will need to be typed:
"

```
#!batch

cd /to/your/folder/location
jar -cvf yourName.war *
```

"
(replace /to/your/folder/location with the location of your folder)
(replace yourName.war with the name you want to give the file.)

-put the .war file into your tomcat %CATALINA_HOME%\webapps folder.

-Now run %CATALINA_HOME%\bin\startup.bat.

-Now open localhost:8080/yourfolderName/yourProjectName

-Everything is set to work.




### Aditional info and sources ###

Molecular weight calculated used the guidelines of: http://www.bio-protocol.org/e46 (24/01/2017)
Some of the preset restriction enzymes used from: https://www.neb.com/tools-and-resources/selection-charts/alphabetized-list-of-recognition-specificities (24/01/2017)