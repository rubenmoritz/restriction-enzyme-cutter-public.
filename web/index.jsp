<%-- 
    Document   : cutter
    Created on : Jan 7, 2017, 12:48:58 PM
    Author     : ruben
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%--<%@ page import="RegexFunctions" %>---%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Restriction enzyme cutter.</title>
        <!-- Latest Jquery -->
        <script
            src="http://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- bootstrap selecter -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
        <!-- functions enzyme cutter specific-->
        <script type="text/javascript" src="opmaakEnFuncties/functions.js"></script>

    </head>
    <body>
        <div style=" width: 80%; float: left; padding-left:10%">
            <h1>Restriction enzyme cutter.</h1>
            <div id="enzymeinp" class=" panel panel-default" style="width: 100%;">
                <div class="panel-heading">Select or type in your restriction enzyme sequence:</div>
                <div class="panel-body input-group">
                    <!-- Selecter with search function adds function to add predefined enzymes into the textbox.-->
                    <select style="width: 100%;" class="selectpicker" data-size="8"  data-live-search="true" id="sel1">
                        <option value="AA/CGTT">AclI</option>
                        <option value="A/AGCTT">HindIII HindIII-HF</option>
                        <option value="AAT/ATT">SspI SspI-HF</option>
                        <option value="/AATT">MluCI Tsp509I</option>
                        <option value="A/CATGT">PciI</option>
                        <option value="A/CCGGT">AgeI AgeI-HF</option>
                        <option value="A/CCWGGT">SexAI</option>
                        <option value="A/CGCGT">MluI MluI-HF</option>
                        <option value="A/CGT">HpyCH4IV</option>
                        <option value="A/CTAGT">SpeI SpeI-HF</option>
                        <option value="A/GATCT">BglII</option>
                        <option value="AGC/GCT">AfeI</option>
                        <option value="AG/CT">AluI</option>
                        <option value="AGG/CCT">StuI</option>
                        <option value="AGT/ACT">ScaI ScaI-HF</option>
                        <option value="AT/CGAT">ClaI BspDI</option>
                        <option value="ATGCA/T">NsiI NsiI-HF</option>
                        <option value="AT/TAAT">AseI</option>
                        <option value="ATTT/AAAT">SwaI</option>
                        <option value="C/AATTG">MfeI MfeI-HF</option>
                        <option value="CAC/GTG">PmlI</option>
                        <option value="CAG/CTG">PvuII PvuII-HF</option>
                        <option value="CA/TATG">NdeI</option>
                        <option value="CATG/">NlaIII</option>
                        <option value="C/ATG">CviAII</option>
                        <option value="/CATG">FatI</option>
                        <option value="C/CATGG">NcoI NcoI-HF</option>
                        <option value="CCC/GGG">SmaI</option>
                        <option value="C/CCGGG">XmaI TspMI</option>
                        <option value="CCGC/GG">SacII</option>
                        <option value="C/CGG">MspI HpaII</option>
                        <option value="C/CTAGG">AvrII</option>
                        <option value="CCTGCA/GG">SbfI SbfI-HF</option>
                        <option value="CGAT/CG">PvuI PvuI-HF</option>
                        <option value="CG/CG">BstUI</option>
                        <option value="C/GGCCG">EagI EagI-HF</option>
                        <option value="C/GTACG">BsiWI BsiWI-HF</option>
                        <option value="C/TAG">BfaI</option>
                        <option value="C/TCGAG">XhoI PaeR7I TliI</option>
                        <option value="CTGCA/G">PstI PstI-HF</option>
                        <option value="C/TTAAG">AflII</option>
                        <option value="GAANN/NNTTC">XmnI</option>
                        <option value="G/AATTC">EcoRI EcoRI-HF</option>
                        <option value="GACGT/C">AatII</option>
                        <option value="GAC/GTC">ZraI</option>
                        <option value="GAG/CTC">Eco53kI</option>
                        <option value="GAGCT/C">SacI SacI-HF</option>
                        <option value="GAT/ATC">EcoRV EcoRV-HF</option>
                        <option value="/GATC">MboI Sau3AI DpnII BfuCI</option>
                        <option value="GA/TC">DpnI</option>
                        <option value="GCATG/C">SphI SphI-HF</option>
                        <option value="GCCC/GGGC">SrfI</option>
                        <option value="GCC/GGC">NaeI</option>
                        <option value="G/CCGGC">NgoMIV</option>
                        <option value="GCGAT/CGC">AsiSI</option>
                        <option value="G/CGC">HinP1I</option>
                        <option value="GCG/C">HhaI</option>
                        <option value="G/CGCGC">BssHII</option>
                        <option value="GC/GGCCGC">NotI NotI-HF</option>
                        <option value="G/CTAGC">NheI NheI-HF</option>
                        <option value="GCTAG/C">BmtI BmtI-HF</option>
                        <option value="G/GATCC">BamHI BamHI-HF</option>
                        <option value="GG/CC">HaeIII PhoI</option>
                        <option value="GGCCGG/CC">FseI</option>
                        <option value="GG/CGCC">NarI</option>
                        <option value="G/GCGCC">KasI</option>
                        <option value="GGC/GCC">SfoI</option>
                        <option value="GGCGC/C">PluTI</option>
                        <option value="GG/CGCGCC">AscI</option>
                        <option value="GGGCC/C">ApaI</option>
                        <option value="G/GGCCC">PspOMI</option>
                        <option value="GGTAC/C">KpnI KpnI-HF</option>
                        <option value="G/GTACC">Acc65I</option>
                        <option value="GT/AC">RsaI</option>
                        <option value="G/TAC">CviQI</option>
                        <option value="GTA/TAC">BstZ17I</option>
                        <option value="G/TCGAC">SalI SalI-H</option>
                        <option value="G/TGCAC">ApaLI</option>
                        <option value="GTT/AAC">HpaI</option>
                        <option value="GTTT/AAAC">PmeI</option>
                        <option value="TAC/GTA">SnaBI</option>
                        <option value="T/CATGA">BspHI</option>
                        <option value="T/CCGGA">BspEI</option>
                        <option value="T/CGA">TaqαI</option>
                        <option value="TCG/CGA">NruI NruI-HF</option>
                        <option value="T/CTAGA">XbaI</option>
                        <option value="T/GATCA">BclI</option>
                        <option value="TG/CA">HpyCH4V</option>
                        <option value="TGC/GCA">FspI</option>
                        <option value="TGG/CCA">MscI</option>
                        <option value="T/GTACA">BsrGI BsrGI-HF</option>
                        <option value="T/TAA">MseI</option>
                        <option value="TTAAT/TAA">PacI</option>
                        <option value="TTA/TAA">PsiI</option>
                        <option value="TT/CGAA">BstBI</option>
                        <option value="TTT/AAA">DraI</option>
                    </select>

                    <!-- Enzyme sequence textfield can be used to enter a custom sequence.-->
                    <input class="form-control" type="text" id="enzyme" data-toggle="tooltip" title="Put your restriction enzyme here." name="enzyme" value="AA/CGTT" />
                </div>
                <form>
                    <!-- The total sequence to cut goes here.-->
                    <div id="textinp" class="input-group">
                        <span class="input-group-addon">Sequence to cut:</span>
                        <textarea  type="text" style="max-width: 100%" class="form-control" data-toggle="tooltip" title="Put in your sequence here!" sizename="NucleotideField" id="NucleotideField" value="aaattaaagctagccccaagctatatattaaa" rows="5"></textarea>
                    </div>
                </form>



            </div>
            <!-- Go button.-->
            <input class="btn btn-success" style="width: 100%"  type="button" id="button1" name="button1" value="GO" />
        </div>
        <!-- The div that will hold the future response table is limited to 100% of the screen instead of 80%.-->
        <div id="getResponse"> </div>
    </div>
</body>
</html>
