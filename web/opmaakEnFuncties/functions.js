/* 
 * Contains all functions for changing fields sending requests and checking strings.
 */



/* 
 * init at pageload.
 */
$(document).ready(function () {
    var activated = false;


    /* 
     * Does the GET responder if all information is correct.
     * After this it will replace a dif with a generated table containing all fragments generated.
     * 
     */
    $('#button1').click(function () {
        if (activated) {
            $.ajax({

                type: 'GET',
                url: 'responder',
                data: {
                    NucleotideField: $('#NucleotideField').val(),
                    enzyme: $('#enzyme').val()
                },
                dataType: "text",
                success: function (responseText) {
                    $('#getResponse').replaceWith(responseText);
                }
            });
        }
    });


    /* 
     * checks if the sequence is correct it can only contain ATCG and needs atleast one of these.
     */
    var isValidSequence = function (sequence) {
        var patternGood = new RegExp(/[ATCG]+/i);
        var patternBad = new RegExp(/[0-9 bdefhijklmnopqrsuvwxyz\W\s]+/i);
        if (!patternGood.test(sequence) || patternBad.test(sequence)) {
            return false;
        } else {
            return true;
        }
        return false;
    };


    /* 
     * checks if the button needs to be locked and changed if the restriction seqence or total sequence is not valid.
     */
    function validateSequence(el, val, enz) {
        if (enz === 0) {
            var err = !isValidSequence(val);
        } else if (enz === 1) {
            var err2 = !isValidEnzyme(val);
        }
        if (err) {
            $('#button1').attr('class', 'btn btn-primary disabled');
            $('#button1').attr('data-toggle', 'tooltip');
            $('#button1').attr('title', 'There is a problem with your sequence please recheck.');
            $('#textinp').attr('class', 'input-group has-error');
            activated = false;

        } else if (err2) {
            $('#button1').attr('class', 'btn btn-primary disabled');
            $('#button1').attr('data-toggle', 'tooltip');
            $('#button1').attr('title', 'There is a problem with your enzyme sequence please recheck.');
            $('#enzymeinp').attr('class', 'panel panel-default has-error');
            activated = false;
        } else if (!err && !err2) {
            $('#button1').attr('class', 'btn btn-success');
            $('#button1').attr('data-toggle', 'tooltip');
            $('#button1').attr('title', 'This sequence is correct.');
            $('#textinp').attr('class', 'input-group has-success');
            $('#enzymeinp').attr('class', 'panel panel-default has-success');
            activated = true;
        }
    }

    /* 
     * Checks is the restriction enzyme is valid.
     * This can only contain ATCG and needs to contain atleast one of these.
     * It also needs to contain a / 
     */
    var isValidEnzyme = function (sequence) {
        var patternGood = new RegExp(/[ATCG]+[\//]{1}/i);
        var patternBad = new RegExp(/[0-9 bdefhijklmnopqrsuvwxyz\s]+/i);
        if (!patternGood.test(sequence) || patternBad.test(sequence)) {
            return false;
        } else {
            return true;
        }
        return false;
    };

    $('#sel1').change(function () {
        $('#enzyme').val($('#sel1').val());
    });


    $('#NucleotideField').blur(function () {
        validateSequence(this, this.value, 0);
    });

    $('#NucleotideField').keyup(function () {
        validateSequence(this, this.value, 0);
    });


    $('#enzyme').blur(function () {
        validateSequence(this, this.value, 1);
    });

    $('#enzyme').keyup(function () {
        validateSequence(this, this.value, 1);
    });


});

