/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text.transformations;

import objects.nucleotideString;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ruben
 */
public class TextSearcher {

    /**
     * uses regex to find the corect fragments and puts it in a nucleotideString
     * list. This way it will contain all useful information about the sequence
     * and will be ready to use.
     *
     * @param totalString the total string to analyze.
     * @param restrictionString the restriction enzyme to use.
     * @return a list with all fragments that will occure.
     */
    public List<nucleotideString> generateFragments(String totalString, String restrictionString) {
        totalString = totalString.toUpperCase();
        restrictionString = restrictionString.toUpperCase();
        int locationSlash = restrictionString.indexOf("/");
        System.out.println(restrictionString);
        restrictionString = restrictionString.replace("/", "");
        System.out.println(restrictionString + locationSlash);

        //Regex patern.
        Pattern pattern = Pattern.compile(restrictionString);
        Matcher matcher = pattern.matcher(totalString);
        // Check all occurrences
        List<nucleotideString> frags = new ArrayList<nucleotideString>();
        int lastStart = 0;
        int count = 0;
        //Find amount of fragments.
        while (matcher.find()) {
            count = count + 1;
        }
        //reset matcher to loop over it again.
        matcher.reset();
        int startPos = 0;
        int stopPos = 0;
        String foundNucString;
        //gets all fragments that will ocure and put it in a nucleotideString object list.

        for (int i = 0; i < count; i++) {
            matcher.find();

            if (i < count - 1) {
                foundNucString = totalString.substring(lastStart, matcher.start() + locationSlash);
                startPos = stopPos + 1;
                stopPos = stopPos + foundNucString.length();
                frags.add(new nucleotideString(foundNucString, startPos, stopPos));
            } else {
                foundNucString = totalString.substring(lastStart, matcher.start() + locationSlash);
                startPos = stopPos + 1;
                stopPos = stopPos + foundNucString.length();
                frags.add(new nucleotideString(foundNucString, startPos, stopPos));
                lastStart = matcher.start() + locationSlash;
                foundNucString = totalString.substring(lastStart, totalString.length());
                startPos = stopPos;
                stopPos = stopPos + foundNucString.length();
                frags.add(new nucleotideString(foundNucString, startPos, stopPos));
            }
            lastStart = matcher.start() + locationSlash;
        }
        if (count == 0) {
            foundNucString = totalString.substring(lastStart, totalString.length());
            startPos = 1;
            stopPos = stopPos + foundNucString.length();
            frags.add(new nucleotideString(foundNucString, startPos, stopPos));
        }

        //returns the fragment list.
        return frags;
    }

}
