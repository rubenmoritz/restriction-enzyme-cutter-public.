/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

/**
 *
 * @author ruben This is a custom object that holds all needed information of
 * the nucleotide string.
 */
public class nucleotideString {

    private String totalString;
    private int startInt;
    private int stopInt;
    private int gCount;
    private int cCount;
    private int aCount;
    private int tCount;
    private double gWeight;
    private double cWeight;
    private double aWeight;
    private double tWeight;
    private double gcPercentage;
    private double molWeight;

    /**
     *
     * @return returns start index.
     */
    public int getStartInt() {
        return startInt;
    }

    /**
     *
     * @param startInt set start index.
     */
    public void setStartInt(int startInt) {
        this.startInt = startInt;
    }

    /**
     *
     * @return stop index.
     */
    public int getStopInt() {
        return stopInt;
    }

    /**
     *
     * @param stopInt sets stop index.
     */
    public void setStopInt(int stopInt) {
        this.stopInt = stopInt;
    }

    /**
     *
     * @return total moluculair weight.
     */
    public double getMolWeight() {
        return molWeight;
    }

    /**
     * sets total moluculair weight.
     */
    public void setMolWeight() {
        this.molWeight = gWeight + cWeight + aWeight + tWeight;
    }

    /**
     *
     * @return gets the weight of all G base.
     */
    public double getgWeight() {
        return gWeight;
    }

    /**
     * sets the weight of all G base.
     */
    public void setgWeight() {
        this.gWeight = this.gCount * 329.21;
    }

    /**
     *
     * @return gets the weight of all C base.
     */
    public double getcWeight() {
        return cWeight;
    }

    /**
     * sets the weight of all C base.
     */
    public void setcWeight() {
        this.cWeight = this.cCount * 289.18;
    }

    /**
     *
     * @return gets the weight of all A base.
     */
    public double getaWeight() {
        return aWeight;
    }

    /**
     * sets the weight of all A base.
     */
    public void setaWeight() {
        this.aWeight = this.aCount * 313.21;
    }

    /**
     *
     * @return gets the weight of all T base.
     */
    public double gettWeight() {
        return tWeight;
    }

    /**
     * sets the weight of all T base.
     */
    public void settWeight() {
        this.tWeight = this.tCount * 304.2;
    }

    /**
     *
     * @return gets GC percentage.
     */
    public double getGcPercentage() {
        return gcPercentage;
    }

    /**
     * sets GC percentage.
     */
    public void setGcPercentage() {
        double totalCount = (double) (this.gCount + this.aCount + this.cCount + this.tCount);
        double gcCount = (double) (this.gCount + this.cCount);
        double calculateGCPerc = (gcCount / totalCount) * 100;
        System.out.println("g a c t" + this.gCount + " " + this.aCount + " " + this.cCount + " " + this.tCount);
        System.out.println(gcCount + "gc");
        System.out.println(totalCount + "total");
        System.out.println(calculateGCPerc);
        this.gcPercentage = calculateGCPerc;
    }

    /**
     *
     * @param totalString intelization without start and stop number.
     */
    public nucleotideString(String totalString) {
        this.totalString = totalString.toUpperCase();
        this.startInt = -1;
        this.stopInt = -1;
        setTotalString(totalString);
        setgCount();
        setcCount();
        setaCount();
        settCount();
        setgWeight();
        setcWeight();
        settWeight();
        setaWeight();
        setGcPercentage();
        setMolWeight();
    }

    /**
     *
     * @param totalString intilization with Start and stop number.
     * @param startInt start position in the total sequence.
     * @param stopInt stop position in the total sequence.
     */
    public nucleotideString(String totalString, int startInt, int stopInt) {
        this.totalString = totalString.toUpperCase();
        this.startInt = startInt;
        this.stopInt = stopInt;
        setTotalString(totalString);
        setgCount();
        setcCount();
        setaCount();
        settCount();
        setgWeight();
        setcWeight();
        settWeight();
        setaWeight();
        setGcPercentage();
        setMolWeight();
    }

    /**
     *
     * @param totalString total sequenece
     * @param stringToMatch string to match
     * @return the amount of occurences in the total string.
     */
    public int ocCounter(String totalString, String stringToMatch) {
        int occurrences = 0;
        for (int i = 0; i < totalString.length(); i++) {
            System.out.println(totalString.substring(i, i + 1));
            if (totalString.substring(i, i + 1).contains(stringToMatch)) {
                occurrences++;
            }
        }
        return occurrences;
    }

    /**
     *
     * @return gets the total string.
     */
    public String getTotalString() {
        return totalString;
    }

    /**
     *
     * @param totalString sets the total string
     */
    public void setTotalString(String totalString) {
        this.totalString = totalString.toUpperCase();
    }

    /**
     *
     * @return g count.
     */
    public int getgCount() {
        return gCount;
    }

    /**
     * set g count.
     */
    public void setgCount() {
        int gCounted = ocCounter(totalString, "G");
        this.gCount = gCounted;
    }

    /**
     *
     * @return c count.
     */
    public int getcCount() {
        return cCount;
    }

    /**
     * set c count.
     */
    public void setcCount() {
        int cCounted = ocCounter(totalString, "C");
        this.cCount = cCounted;
    }

    /**
     *
     * @return a count
     */
    public int getaCount() {
        return aCount;
    }

    /**
     * set a count.
     */
    public void setaCount() {
        int aCounted = ocCounter(totalString, "A");
        this.aCount = aCounted;
    }

    /**
     *
     * @return t count.
     */
    public int gettCount() {
        return tCount;
    }

    /**
     * set t count.
     *
     */
    public void settCount() {
        int tCounted = ocCounter(totalString, "T");
        this.tCount = tCounted;
    }

}
