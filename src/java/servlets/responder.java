package servlets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import text.transformations.TextSearcher;
import objects.nucleotideString;

/**
 *
 * @author r.moritz
 */
@WebServlet(urlPatterns = {"/responder"})
public class responder extends HttpServlet {

    /**
     *
     * @param TotalString the total sequence to cut.
     * @param cutter the sequence to use to cut.
     * @return a html5 table to replace a div with on a webpage with all need
     * information suing the TextSearcher.
     */
    protected String makeTableString(String TotalString, String cutter) {
        nucleotideString NS = new nucleotideString(TotalString);
        TextSearcher TS = new TextSearcher();
        String HTMLTableString = new String();
        //out.println("<h1>" + " percentage=" + NS.getGcPercentage() + " weight=" + NS.getMolWeight() + "</h1>");
        List<nucleotideString> fragments = TS.generateFragments(NS.getTotalString(), cutter);
        HTMLTableString
                = "<div class=\"well\" style=\"max-width:100%;\" id=\"getResponse\">"
                + "<table class=\"table table-bordered\">"
                + "<thead>"
                + "<tr>"
                + "<th style=\"max-width: 30%; min-width:10%;word-break:keep-all;\">Full string</th>"
                + "<th style=\"max-width: 30%; min-width:10%;word-break:keep-all;\">Start</th>"
                + "<th style=\"max-width: 30%; min-width:10%;word-break:keep-all;\">Stop</th>"
                + "<th style=\"max-width: 30%; min-width:10%;word-break:keep-all;\">Length</th>"
                + "<th style=\"max-width: 30%; min-width:10%;word-break:keep-all;\">Weight</th>"
                + "<th style=\"max-width: 30%; min-width:10%;word-break:keep-all;\">GC-percentage</th>"
                + "</tr>"
                + "</thead>";
        for (int i = 0; i < fragments.size(); i++) {
            HTMLTableString = HTMLTableString + "<tr>";
            HTMLTableString = HTMLTableString + "<th style=\"max-width: 30%; min-width:10%;word-break:break-all;\">" + fragments.get(i).getTotalString();
            HTMLTableString = HTMLTableString + "<th style=\"max-width: 30%; min-width:10%;\">" + fragments.get(i).getStartInt();
            HTMLTableString = HTMLTableString + "<th style=\"max-width: 30%; min-width:10%;\">" + fragments.get(i).getStopInt();
            HTMLTableString = HTMLTableString + "<th style=\"max-width: 30%; min-width:10%;\">" + fragments.get(i).getTotalString().length();
            HTMLTableString = HTMLTableString + "<th style=\"max-width: 30%; min-width:10%;\">" + fragments.get(i).getMolWeight();
            HTMLTableString = HTMLTableString + "<th style=\"max-width: 30%; min-width:10%;\">" + fragments.get(i).getGcPercentage();
            HTMLTableString = HTMLTableString + "</tr>";
        }
        HTMLTableString = HTMLTableString + "</table>"
                + "</div>";
        return HTMLTableString;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request gets information from the webpage through
     * ajax.
     * @param response servlet response sends back the information generated in
     * the makeTableString function.
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String dataToUse = request.getParameter("NucleotideField").trim();
        String cutter = request.getParameter("enzyme").trim();

        String outputData = makeTableString(dataToUse, cutter);

        response.setContentType("text/html");
        response.getWriter().write(outputData);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
